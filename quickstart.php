<?php
require __DIR__ . '/vendor/autoload.php';

// if (php_sapi_name() != 'cli') {
//     throw new Exception('This application must be run on the command line.');
// }

/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
function getClient()
{
    $client = new Google_Client();
    $client->setApplicationName('Google Sheets API PHP Quickstart');
    $client->setScopes(Google_Service_Sheets::SPREADSHEETS_READONLY);
    $client->setAuthConfig('credentials.json');
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');

    // Load previously authorized token from a file, if it exists.
    // The file token.json stores the user's access and refresh tokens, and is
    // created automatically when the authorization flow completes for the first
    // time.
    $tokenPath = 'token.json';
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    }

    // If there is no previous token or it's expired.
    if ($client->isAccessTokenExpired()) {
        // Refresh the token if possible, else fetch a new one.
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
            $client->setAccessToken($accessToken);

            // Check to see if there was an error.
            if (array_key_exists('error', $accessToken)) {
                throw new Exception(join(', ', $accessToken));
            }
        }
        // Save the token to a file.
        if (!file_exists(dirname($tokenPath))) {
            mkdir(dirname($tokenPath), 0700, true);
        }
        file_put_contents($tokenPath, json_encode($client->getAccessToken()));
    }
    return $client;
}

function sort_objects_by_datetime($a, $b) {
	if($a['donate_datetime'] == $b['donate_datetime']){ return 0 ; }
	return ($a['donate_datetime'] > $b['donate_datetime']) ? -1 : 1;
}

function get_time_ago( $time )
{
    $time_difference = time() - $time;

    if( $time_difference < 1 ) { return 'less than 1 second ago'; }
    $condition = array( 12 * 30 * 24 * 60 * 60 =>  'year',
                30 * 24 * 60 * 60       =>  'month',
                24 * 60 * 60            =>  'day',
                60 * 60                 =>  'hour',
                60                      =>  'minute',
                1                       =>  'second'
    );

    foreach( $condition as $secs => $str )
    {
        $d = $time_difference / $secs;

        if( $d >= 1 )
        {
            $t = round( $d );
            return 'about ' . $t . ' ' . $str . ( $t > 1 ? 's' : '' ) . ' ago';
        }
    }
}
// Get the API client and construct the service object.
$client = getClient();
$service = new Google_Service_Sheets($client);

$spreadsheetId = '1v__LyJYtFhqA3O9S3JLC_9nJh1Mb0ZDkM2JqSmZTn5Q';
$range = 'Sheet1!A2:E';
$response = $service->spreadsheets_values->get($spreadsheetId, $range);
$values = $response->getValues();

if (empty($values)) {
    echo "No data found.\n";
} else {
    $result = [];
    // usort($values, 'sort_objects_by_total')
    foreach ($values as $row) {
        $one_data = [
            "name" => $row[0],
            "donate" => rand(30, 250),
            "donate_datetime" =>  strtotime($row[3])
        ];
        array_push($result, $one_data);
    }
    usort($result, 'sort_objects_by_datetime');
    $recent_data = array_slice($result, 0, 10);
 }
 ?>
 <div class="pr-sm-12">
    <h3 class="text--no-case heading--small">Recent Supporters</h3>
    <p><span id="donorCounter">This campaign currently has <span id="counter" data-count="779">0</span> donors!</span></p>
    <ul id="recentsupporters">
        <?php
            foreach ($recent_data as $data ) {
                echo "<li><img src='https://letsendhardshiptoday.com/wp-content/uploads/2019/11/end75.png'>" . $data['name'] . "<br>$" . $data['donate'] . "-" . get_time_ago($data['donate_datetime']) . "...</li>";
            }
        ?>
    </ul>
</div>


<style>
    h3.heading--small {
        font-size: 2rem;
    }
    #donorCounter {
        background-color: #E0B250;
        padding: 10px;
        border-radius: 5px;
    }
    #recentsupporters {
        list-style-type: none;
    }
    #recentsupporters li:not(:last-child) {
        margin-bottom: 5px;
    }
    #recentsupporters img {
        width: 32px;
        height: 32px;
        float: left;
        margin-right: 17px;
    }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>
    var $this = $(counter),
    countTo = $this.attr('data-count');
  
    $({ countNum: $this.text()}).animate({
        countNum: countTo
    },
    {
        duration: 1000,
        easing:'linear',
        step: function() {
            $this.text(Math.floor(this.countNum));
        },
        complete: function() {
            $this.text(this.countNum);
        }
    });    
</script>